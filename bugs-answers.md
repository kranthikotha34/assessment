Please type your answers out here

1.1.1 First repeated character = a
1.1.2 First repeated character = o
1.1.3 No repeated character.
1.1.4 No repeated character.

1.2.1 "aardvark" First repeated character = a
1.2.2 "roommate" First repeated character = o
1.2.3 "mate" java.lang.StringIndexOutOfBoundsException: String index out of range: 4
1.2.4 "test" java.lang.StringIndexOutOfBoundsException: String index out of range: 4

1.3   Yes, there are bugs in `firstRepeatedCharacter` exception is java.lang.StringIndexOutOfBoundsException: String index out of range: 4. to solve this we need to fix in for loop as follows:

		public char firstRepeatedCharacter()
	   {
	      for (int i = 0; i < (word.length())-1; i++)
	      {
	         char ch = word.charAt(i);
	         if (ch == word.charAt(i + 1))
	            return ch;
	      }
	      return 0;
	   }

1.4   If we pass 'null' for string then it will raises null pointer exception. to solve this we need you exception handling with try catch or we check null condition by using if as follows:
			
			public char firstRepeatedCharacter()
		   {
			   if(word!=null)
			   {
			      for (int i = 0; i < (word.length())-1; i++)
			      {
			         char ch = word.charAt(i);
			         if (ch == word.charAt(i + 1))
			            return ch;
			      }
			   }
		      return 0;
		   }
		

2.1.1 First multiple character = i
2.1.2 No multiple character.
2.1.3 No multiple character.

2.2.1 First multiple character = m
2.2.2 First multiple character = m
2.2.3 First multiple character = t

2.3   Yes, there are bugs in the code as it will not returning output correctly so i have changed the code as below to get the output as we need.

		public char firstMultipleCharacter()
	   {
	      for (int i = 0; i < word.length(); i++)
	      {
	         char ch = word.charAt(i);
	         if (find(ch, i) >= 0)
	            return ch;
	      }
	      return 0;
	   }

	   private int find(char c, int pos)
	   {
	      for (int i = pos+1; i < word.length(); i++)
	      {
	         if (word.charAt(i) == c) 
	         {
	            return i;
	         }
	      }
	      return -1;
	   }

3.1.1 2 repeated characters.
3.1.2 0 repeated characters.
3.1.3 4 repeated characters.

3.2.1 "missisippi" 2 repeated characters.
3.2.2 "test" 0 repeated characters.
3.2.3 "aabbcdaaaabb" 3 repeated characters.

3.3   Yes, there are bugs in the code as it will not returning output correctly so i have changed the code as below to get the output as we need.

		public int countRepeatedCharacters()
	   {
	      int c = 0;
	      for (int i = 0; i < word.length() - 1; i++)		// here i have changed i value to 0
	      {
	         if (word.charAt(i) == word.charAt(i + 1)) // found a repetition
	         {
	            if ( i==0 || word.charAt(i - 1) != word.charAt(i)) // here i have added one condition for checking i =0 because other wise it won't count the first group starts at first index itself 
	               c++;
	         }
	      }     
	      return c;
	   }

4.    I have used breakpoints to debug the code.

5.    I don't have much knowledge on design patterns but i have basic idea from those concepts i have used only single ton methodology for creating singleton classes for connecting external resources like database connections etc.,