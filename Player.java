package com.assessment;

import java.util.List;

public class Player {
	
	private Integer currentstatus;
	private Integer previousstatus;
	private String statusmsg = "";
	private static Integer count = 0;
	
	Player(Integer val){
		currentstatus = val;
	}
	
	Player(){
		currentstatus = 0;
	}
	
	public Integer getCurrentstatus() {
		return currentstatus;
	}
	public void setCurrentstatus(Integer currentstatus) {
		this.currentstatus = currentstatus;
	}
	public Integer getPreviousstatus() {
		return previousstatus;
	}
	public void setPreviousstatus(Integer previousstatus) {
		this.previousstatus = previousstatus;
	}
	
	public void dicethrown()
	{
		count++;
		Integer[] dicevalues = new Integer[2];
		dicevalues[0] = DiceThrown.diceValue();
		dicevalues[1] = DiceThrown.diceValue();
		Integer previousval = 0;
		previousval = previousval+dicevalues[0]+dicevalues[1];
		if(currentstatus>96 && currentstatus<=99 && (currentstatus+previousval)<100)
		{
			int temp = 100-currentstatus;
			currentstatus = currentstatus-(previousval-temp);
		}
		else
		{
				while(dicevalues[0]==dicevalues[1])
				{
					previousval = previousval+dicevalues[0]+dicevalues[1];
					dicevalues[0] = DiceThrown.diceValue();
					dicevalues[1] = DiceThrown.diceValue();
				}
				previousstatus = currentstatus;
				currentstatus = currentstatus+previousval;
		}
		if(currentstatus>100)
		{
			currentstatus = previousstatus;
		}
		
		List getvalue = Board.getvalue(currentstatus);
		currentstatus = (Integer) getvalue.get(0);
		statusmsg = (String) getvalue.get(1);
	}

	public String getStatusmsg() {
		return statusmsg;
	}

	public void setStatusmsg(String statusmsg) {
		this.statusmsg = statusmsg;
	}

	public static Integer getCount() {
		return count;
	}

	public static void setCount(Integer count) {
		Player.count = count;
	}
	
	
	

}
